## O
- During code review we looked at our classmates' implementations of the strategy pattern for their weekend assignments.
- Each group introduces a design pattern, the Strategy Pattern, the Observer Pattern, and the Command Pattern. We learned about many examples of these design patterns and compared the Strategy Pattern with the Command Pattern.
- We learned how to determine if code needs refactoring and how to go about refactoring code. Code refactoring includes find code smell, refactor techniques, commit by baby step. 
- The entire refactoring process is done with the assurance that there are tests that will keep the functionality of the code intact during the refactoring process.
- In class we practiced how to find code smells with many examples, like duplicate code, cryptic naming, overly long methods, and so on. At the end of the class we modified a project that calculates the frequency of words as a homework assignment.
## R
- I was very confused when I saw the code in the assignment. 
## I
- Gained a deeper understanding of the three design patterns that were discussed.
- Careful consideration needs to be given to the scenario and the need to use the design pattern before using it.
- The code in the assignment was just too hard to read and realized the importance of writing readable code.
## D
- I need to develop a good code habit.