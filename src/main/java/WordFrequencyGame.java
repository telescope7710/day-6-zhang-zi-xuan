import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {
    public static final String SPLIT_REGEX = "\\s+";
    public static final String EXCEPTION_MESSAGE = "Calculate Error";

    public String inputWordFrequencyResult(String inputStr) {
        try {
            Map<String, Integer> wordFrequencyMap = countWordFrequencyMap(inputStr);
            List<Word> sortedWordFrequencyList = sortWordFrequencyList(wordFrequencyMap);
            return generateWordFrequencyResult(sortedWordFrequencyList);
        } catch (Exception e) {
            return EXCEPTION_MESSAGE;
        }
    }

    private String generateWordFrequencyResult(List<Word> sortedWordFrequencyList) {
        return sortedWordFrequencyList.stream().map(word -> word.getWord() + " " + word.getWordCount()).collect(Collectors.joining("\n"));
    }

    private List<Word> sortWordFrequencyList(Map<String, Integer> wordFrequencyMap) {
        return wordFrequencyMap
                .entrySet()
                .stream()
                .sorted((word1, word2) -> word2.getValue().compareTo(word1.getValue()))
                .map(word -> new Word(word.getKey(), word.getValue()))
                .collect(Collectors.toList());
    }

    private Map<String, Integer> countWordFrequencyMap(String inputStr) {
        return Arrays.stream(inputStr.split(SPLIT_REGEX))
                .collect(Collectors.toMap(inputSplitString -> inputSplitString, inputSplitString -> 1, Integer::sum));
    }
}
